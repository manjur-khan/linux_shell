#ifndef SFISH_H
#define SFISH_H
#include <readline/readline.h>
#include <readline/history.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>
#include "parser.h"
#include <signal.h>
#endif

struct allGlobalVariable {
	int save_stdin;
	int save_stdout;
	int save_stderr;
	int updateDir;
	int redirect;
	int now_pid;
	int timer;
	int child_pid;
	char* last_dir_visited;
	char* current_directory;
	char** home_directory_broken;
	char** path_broken;
	int redirect_count;
	Redirect** redirects;
};

typedef struct allGlobalVariable GlobalVariable;

#define CALLOC_SIZE 1024

int call_cd(char* path);
int call_help();
int call_pwd();
int call_exit();

int distributeArgument(GlobalVariable* globalVariable, char* envp[]);


char* getCurrentDir(char* save);

// Signal handlers
void handle_ctrl_c(int signo);
void handle_easy(int signo);
void handle_child_death(int signo);
void do_nothing_sig(int sogno);
void print_alarm(int signo);
