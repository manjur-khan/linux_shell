#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include "debug.h"

#define MAX_BUFFER_SIZE 1024

/**
	@brief This function takes the input and makes an array of words
	@description The line is read and an array of words is returned.
			The parse takes place where there is space or special characters.
			It is mainly used to saperate the commands from the user so then can be treated accordingly.
	@return returns an array of string.
*/
char** getParsedArray(char* input);
/**
	@brief This function takes the directory name and parses it into every subfolder.
	@return array of parsed string
*/
char **parseToArray(char* directory, char* parseString);

struct _Redirect {
	char* input;
	char* output;
	char** args;
	int redirect; // 0 = > ; 1 = 1> ; 2 = 2>; 3 = &> ; 4 = >>
	int insert; // 0 = < ; 1 = <<
};
typedef struct _Redirect Redirect;

Redirect** getAdvancedArray(char* input);
