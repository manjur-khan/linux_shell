#include "parser.h"

Redirect** getAdvancedArray(char* input) {
	int buffer = MAX_BUFFER_SIZE, counter = 0;
	if(!input) {
		perror("error: No input found");
		return NULL;
	}
	char** pipeArray = calloc(buffer , sizeof(char*));

	char* token;
		token = strtok(input, "|");

	while(token != NULL) {
		pipeArray[counter] = token;
		counter++;
		if(counter >= buffer) {
			buffer += MAX_BUFFER_SIZE;
			pipeArray = realloc(pipeArray, buffer * sizeof(char*));
			if(!pipeArray) {
				perror("error: Problem with realloc");
				return NULL;
			}
		}
		token = strtok(NULL, "|");
	}
	Redirect **redirect = calloc(buffer, sizeof(Redirect*));
	counter = 0;
	// TODO counter++;
	while(pipeArray[counter] != NULL) {
		int quote = 0;
		char *tempLine = calloc(buffer, sizeof(char));
		int j = 0, k = 0;
		char d = pipeArray[counter][j];
		char parse = '^';
		redirect[counter] = malloc(sizeof(Redirect));
		redirect[counter]->args = calloc(buffer, sizeof(char*));
		redirect[counter]->input = calloc(MAX_BUFFER_SIZE, sizeof(char));
		redirect[counter]->output = calloc(MAX_BUFFER_SIZE, sizeof(char));
		while(d != '\0') {
			if(d == '"') {
				if(quote == 1) {
					quote = 0;
					tempLine[k] = parse;
				}
				if(quote == 0) {
					quote = 1;
					tempLine[k] = parse;
				}
			} else if(d == ' ') {
				if(quote == 0) {
					tempLine[k] = parse;
				} else {
					tempLine[k] = d;
				}
			} else if(d == '>') {
				if(quote == 0) {
					if(pipeArray[counter][j + 1] == '>') {
						redirect[counter]->redirect = 4;
						tempLine[k] = parse; k++;
						tempLine[k] = d; k++; j++;
						d = pipeArray[counter][j];
						tempLine[k] = d; k++;
						tempLine[k] = parse;
					} else {
						redirect[counter]->redirect = 0;
						tempLine[k] = parse; k++;
						tempLine[k] = d; k++;
						tempLine[k] = parse;
					}
				} else {
					tempLine[k] = d;
				}
			} else if(d == '<') {
				if(quote == 0) {
					if(pipeArray[counter][j + 1] == '<') {
						redirect[counter]->insert = 1;
						tempLine[k] = parse; k++;
						tempLine[k] = d; k++; j++;
						d = pipeArray[counter][j];
						tempLine[k] = d; k++;
						tempLine[k] = parse;
					} else {
						redirect[counter]->redirect = 0;
						tempLine[k] = parse; k++;
						tempLine[k] = d; k++;
						tempLine[k] = parse;
					}
				} else {
					tempLine[k] = d;
				}
			} else if(d == '1') {
				if(quote == 0) {
					if(j > 0 && pipeArray[counter][j + 1] == '>' && pipeArray[counter][j - 1] == ' ') {
						redirect[counter]->redirect = 1;
						tempLine[k] = parse; k++;
						tempLine[k] = d; k++; j++;
						d = pipeArray[counter][j];
						tempLine[k] = d; k++;
						tempLine[k] = parse;
					} else {
						tempLine[k] = d;
					}
				} else {
					tempLine[k] = d;
				}
			} else if(d == '2') {
				if(quote == 0) {
					if(j > 0 && pipeArray[counter][j + 1] == '>' && pipeArray[counter][j - 1] == ' ') {
						redirect[counter]->redirect = 2;
						tempLine[k] = parse; k++;
						tempLine[k] = d; k++; j++;
						d = pipeArray[counter][j];
						tempLine[k] = d; k++;
						tempLine[k] = parse;
					} else {
						tempLine[k] = d;
					}
				} else {
					tempLine[k] = d;
				}
			} else if(d == '&') {
				if(quote == 0) {
					if(j > 0 && pipeArray[counter][j + 1] == '>') {
						redirect[counter]->redirect = 3;
						tempLine[k] = parse; k++;
						tempLine[k] = d; k++; j++;
						d = pipeArray[counter][j];
						tempLine[k] = d; k++;
						tempLine[k] = parse;
					}
				} else {
					tempLine[k] = d;
				}
			} else {
				tempLine[k] = d;
			}
			j++; k++;
			if(k >= buffer) {
				tempLine = realloc(tempLine, (k + MAX_BUFFER_SIZE) * sizeof(char));
			}
			d = pipeArray[counter][j];
		}
		char *token = NULL;
		j = 0;
		token = strtok(tempLine, "^");
		int noArg = 0;

		while(token != NULL) {
			//printf("%s\n", token);
			if(strcmp(token, ">") == 0 || strcmp(token, ">>") == 0) {
				noArg = 2; // 2 = output file 1 = input file
			} else if(strcmp(token, "1>") == 0 || strcmp(token, "2>") == 0) {
				noArg = 2; // 2 = output file 1 = input file
			} else if(strcmp(token, "&>") == 0) {
				noArg = 2; // 2 = output file 1 = input file
			}else if(strcmp(token, "<") == 0 || strcmp(token, "<<") == 0) {
				noArg = 1; // 2 = output file 1 = input file
			}

			if(noArg != 0) {
				token = strtok(NULL, "^");
			}

			if(noArg == 1) {
				strcpy(redirect[counter]->input, token);
				//redirect[counter]->input = token;
			}else if(noArg == 2) {
				strcpy(redirect[counter]->output, token);
				//redirect[counter]->output = token;
			} else {
				redirect[counter]->args[j] = calloc(MAX_BUFFER_SIZE, sizeof(char));
				strcpy(redirect[counter]->args[j], token);
				//redirect[counter]->args[j] = token;
				j++;
			}
			if(j >= buffer) {
				buffer += MAX_BUFFER_SIZE;
				redirect[counter]->args = realloc(redirect[counter]->args, buffer * sizeof(char*));
				if(!redirect[counter]->args) {
					//fprintf(stderr, "Problem with realloc at counter %d buffer size %d\n", j, buffer);
					perror("error:");
					return NULL;
				}
			}
			token = strtok(NULL, "^");
		}

		free(tempLine);
		counter++;
	}
	free(pipeArray);
	return redirect;
}

char** getParsedArray(char* input) {
	// buffer keeps track of the buffer size of realloc
	// counter keeps track of the number of token that is being added
	int buffer = MAX_BUFFER_SIZE, counter = 0;
	if(!input) {
		perror("error: Problem with realloc");
		return NULL;
	}
	char** toReturn = NULL; // To return will be returning the array that will later be checked for evaluation
	toReturn = calloc(buffer , sizeof(char*));
	if(!toReturn) {
		perror("error: Problem with realloc");
		return NULL;
	}
	char* parseString = " \n\t\r";
	char* token = NULL;

	//int quote = 0; // is used to see if there is a quotation mark exists
	//int addWord = 0;
	//int addChar = 0;
	// Break the line in parts that it qualifies
	// Breaking the line with spaces. This part I am assuming all the input is given with proper spacing
	/*char c = *input;
	while(c != '\0' && c != EOF) {
		info("while %c = token = %s\n", c, token);
		switch(c) {
			addWord = 0;
			addChar = 0;
			case '"' :
				info("while %c = token = %s\n", c, token);
				if(quote == 0) {
					quote = 1;
				}
				else {
					quote = 0;
					addWord = 1;
				}
				break;
			case ' ' :
				info("while %c = token = %s\n", c, token);
				if(quote == 0) {
					addChar = 0;
				}
				break;
			case '>' :
				info("while %c = token = %s\n", c, token);
				if (quote == 1) {
					addWord = 1;
				} else {
					addWord = 1;
					addChar = 1;
				}
				break;
			case '<' :
				info("while %c = token = %s\n", c, token);
				if (quote == 1) {
					addWord = 1;
				} else {
					addWord = 1;
					addChar = 1;
				}
				break;
			case '|' :
				info("while %c = token = %s\n", c, token);
				if (quote == 1) {
					addWord = 1;
				} else {
					addWord = 1;
					addChar = 1;
				}
				break;
			case '&' :
				info("while %c = token = %s\n", c, token);
				if (quote == 1) {
					addWord = 1;
				} else {
					addWord = 1;
					addChar = 1;
				}
				break;
			default :
				info("while %c = token = %s\n", c, token);
				addWord = 0;
				addChar = 0;
		}
		if(addWord == 0) {
			info("while %c = token = %s\n", c, token);
			strncat(token, &c, 1);
			info("while %c = token = %s\n", c, token);
		}
		if(addWord == 1) {
			info("while %c = token = %s\n", c, token);
			strncat(token, "\0", 1);
			toReturn[counter] = token;
			counter++;
			if (counter >= buffer) {
				buffer += MAX_BUFFER_SIZE;
				toReturn = realloc(toReturn, buffer * sizeof(char*));
				if(!toReturn) {
					fprintf(stderr, "Problem with realloc at counter %d buffer size %d\n", counter, buffer);
					return NULL;
				}
			}
			token = "\0";
		}
		if(addChar == 1) {
			info("while %c = token = %s\n", c, token);
			strncat(token, &c, 1);
			strncat(token, "\0", 1);
			toReturn[counter] = token;
			counter++;
			if (counter >= buffer) {
				buffer += MAX_BUFFER_SIZE;
				toReturn = realloc(toReturn, buffer * sizeof(char*));
				if(!toReturn) {
					fprintf(stderr, "Problem with realloc at counter %d buffer size %d\n", counter, buffer);
					return NULL;
				}
			}
			token = "\0";
		}
	}
	toReturn[counter] = NULL;*/
	token = strtok(input, parseString);

	while(token != NULL) {
		toReturn[counter] = token;
		counter++;
		if(counter >= buffer) {
			buffer += MAX_BUFFER_SIZE;
			toReturn = realloc(toReturn, buffer * sizeof(char*));
			if(!toReturn) {
				perror("error: Problem with realloc");
				return NULL;
			}
		}
		token = strtok(NULL, parseString);
	}

	return toReturn;
}

/** TODO
		Fix this part to take into consideration of no spaces.
			What happens if the user types something like ls>meaw.txt This is valid.
*/

char **parseToArray(char* directory, char* parseString) {
	int buffer = MAX_BUFFER_SIZE, counter = 0;
	char** toReturn = NULL; // To return will be returning the array that will later be checked for evaluation
	toReturn = calloc(buffer, sizeof(char*));
	if(!toReturn) {
		perror("Error: Problem with calloc\n");
		return NULL;
	}
	char* token = NULL;
	token = strtok(directory, parseString);

	while(token != NULL) {
		toReturn[counter] = token;
		counter++;
		if(counter >= buffer) {
			buffer += MAX_BUFFER_SIZE;
			toReturn = realloc(toReturn, buffer * sizeof(char*));
			if(!toReturn) {
				perror("error: Problem with realloc");
				return NULL;
			}
		}
		token = strtok(NULL, parseString);
	}
	return toReturn;
}
