#include "sfish.h"

int currentPID;
GlobalVariable* globalVariable2;

int call_help() {
	char* helper = "Sfish, version 1.0.0-release (Manjur Khan Edition)\n" \
		"These shell commands are defined internally.  Type `help' to see this list.\n" \
		"alarm [timer]\n" \
		"cd [dir]\n" \
		"exit\n" \
		"help\n" \
		"pwd\n";
	printf("%s", helper);
	return 0;
}

int distributeArgument(GlobalVariable* globalVariable, char* envp[]) {
	globalVariable2 = globalVariable;
	globalVariable->updateDir = 0;
	int counting;
	if(globalVariable->redirects == NULL) {
		return 0;
	}
	//pid_t p[globalVariable->redirect_count][2];
	for(counting = 0; counting < globalVariable->redirect_count; counting++) {
		int usr_in = 0, usr_out = 1, usr_err = 2, status;
		if(globalVariable->redirects[counting]->args == NULL) {
			return 0;
		}
		/*if(globalVariable->redirect_count > 1) {
			pipe(p[counting]);
		}*/

		if(globalVariable->redirects[counting]->input != NULL && globalVariable->redirects[counting]->input[0] != '\0') {
			usr_in = open(globalVariable->redirects[counting]->input, O_RDONLY);
			if(usr_in == -1) {
				perror("error: ");
				return 0;
			}
		}
		/*if (globalVariable->redirect_count > 1) {
			if(counting != 0) {
				dup2(p[counting][0],usr_in);
			} else {
				close(p[counting][0]);
			}
		}*/
		if(globalVariable->redirects[counting]->output != NULL && globalVariable->redirects[counting]->output[0] != '\0') {
			if(globalVariable->redirects[counting]->redirect == 4) {
				usr_out = open(globalVariable->redirects[counting]->output, O_RDWR|O_APPEND);
			} else {
				usr_out = open(globalVariable->redirects[counting]->output, O_WRONLY|O_CREAT, 0644);
			}
			if(usr_out == -1) {
				perror("error: ");
				return 0;
			}
			if(globalVariable->redirects[counting]->redirect == 3) {
				usr_err = usr_out;
			}
		}
		/*if (globalVariable->redirect_count > 1) {
			if(counting == globalVariable->redirect_count - 1) {
				close(p[counting][1]);
			} else {
				usr_out = p[counting][1];
				//dup2(p[counting][1], usr_out);
			}
			int q;
			for(q = 0; q <= counting; q++) {
				close(p[q][0]);
				close(p[q][1]);
			}
		}*/
		if(strcmp(globalVariable->redirects[counting]->args[0], "help") == 0) {
			pid_t pid;
			pid = fork();
			if(pid == 0) {
				if(usr_in != 0) {
					dup2(usr_in, 0);
					close(usr_in);
				}
				if(usr_out != 1) {
					dup2(usr_out, 1);
					close(usr_out);
				}
				if(usr_err!= 2) {
					dup2(usr_err, 2);
					close(usr_err);
				}
				call_help();

				exit(0);
			} else if (pid < 0){
				perror("Error");
				return -1;
			} else {
				globalVariable->now_pid = pid;
				dup2(globalVariable->save_stdin, 0);
				dup2(globalVariable->save_stdout, 1);
				dup2(globalVariable->save_stderr, 2);
				waitpid(pid,&status,0);
				return 0;
			}
			dup2(globalVariable->save_stdout, 1);
			return 0;
		} else if (strcmp(globalVariable->redirects[counting]->args[0], "cd") == 0) {
			// TODO add cd - [User's last working directory]
			char *tempOld = NULL;
			tempOld = getCurrentDir(tempOld);
			if(globalVariable->redirects[counting]->args[1] == NULL) {
				int num = chdir(getenv("HOME"));
				if(num == -1) {
					perror("Error");
					return -1;
				}
			}else if (strcmp(globalVariable->redirects[counting]->args[1], "-") == 0) {
				//printf("Last dir visisted %s\n", globalVariable->last_dir_visited);
				if(chdir(globalVariable->last_dir_visited) == -1) {
					perror("Error");
					return -1;
				}
			} else {
				if(chdir(globalVariable->redirects[counting]->args[1]) == -1) {
					perror("Error");
					return -1;
				}
			}
			strcpy(globalVariable->last_dir_visited, tempOld);
			free(tempOld);
			globalVariable->updateDir = 1;
			return 0;
		} else if (strcmp(globalVariable->redirects[counting]->args[0], "pwd") == 0) {
			//int closeFD = 0;
			pid_t pid;
			pid = fork();
			if(pid == 0) {
				if(usr_out != 1) {
					dup2(usr_out, 1);
					close(usr_out);
				}
				printf("%s\n", globalVariable->current_directory);

				exit(0);
			} else if (pid < 0){
				perror("Error");
				return -1;
			} else {
				globalVariable->now_pid = pid;
				dup2(globalVariable->save_stdout, 1);
				waitpid(pid,&status,0);
				return 0;
			}
			dup2(globalVariable->save_stdout, 1);
			return 0;
		} else if(strcmp(globalVariable->redirects[counting]->args[0], "alarm") == 0) {
			if(globalVariable->redirects[counting]->args[1] != NULL) {
				int timer = atoi(globalVariable->redirects[counting]->args[1]);
				//printf("%s\n", globalVariable->redirects[counting]->args[0]);
				globalVariable->timer = timer;
				alarm(timer);

			}
		} else {
			int foundPath = 0;
			char* executePath = calloc(CALLOC_SIZE, sizeof(char));
			struct stat buf;
			if(globalVariable->redirects[counting]->args[0][0] != '.' && globalVariable->redirects[counting]->args[0][0] != '/') {

				int count = 0;
				while(globalVariable->path_broken[count] != NULL) {
					char *tempNewPath = calloc(CALLOC_SIZE, sizeof(char));
					strcpy(tempNewPath, globalVariable->path_broken[count]);
					strcat(tempNewPath, "/");
					strcat(tempNewPath, globalVariable->redirects[counting]->args[0]);
					if(stat(tempNewPath, &buf) == -1) {
						foundPath = 0;
						//printf("file is not here = %s\n", tempNewPath);
					} else {
						foundPath = 1;
						strcpy(executePath, tempNewPath);
						break;
					}
					count++;
					free(tempNewPath);
				}
				if(foundPath == 0) {
					char* tempNewPath = NULL;
					tempNewPath = getCurrentDir(tempNewPath);
					strcat(tempNewPath, "/");
					strcat(tempNewPath, globalVariable->redirects[counting]->args[0]);
					//printf("%s: command not found\n", globalVariable->new_args[0]);
					if(stat(tempNewPath, &buf) == -1) {
						foundPath = 0;
						//printf("file is not here = %s\n", tempNewPath);
					} else {
						foundPath = 1;
						strcpy(executePath, tempNewPath);
					}
					free(tempNewPath);
				}
				if(foundPath == 0) {
					printf("%s: command not found\n", globalVariable->redirects[counting]->args[0]);
					free(executePath);
					return 0;
				}
				globalVariable->save_stdout = dup(1); // stdout = 1
				pid_t pid;
				pid = fork();
				usr_out = 1;
				if(pid == 0) {
					if(usr_in != 0) {
						dup2(usr_in, 0);
						close(usr_in);
					}
					if(usr_out != 1) {
						dup2(usr_out, 1);
						close(usr_out);
					}
					if(usr_err != 2) {
						dup2(usr_err, 2);
						close(usr_err);
					}
					//printf("%s: Excev path \n", executePath);
					// This is where I am executing new program
					if(execve(executePath, globalVariable->redirects[counting]->args , envp) == -1) {
						perror("error");
					}
					exit(0);
				} else if (pid < 0){
					perror("error");
					free(executePath);
					return -1;
				} else {
					globalVariable->now_pid = pid;
					dup2(globalVariable->save_stdout, 1);
					waitpid(pid,&status,0);
				}
				free(executePath);
				dup2(globalVariable->save_stdout, 1);
				return 0;
			} else if(globalVariable->redirects[counting]->args[0][0] == '.' && globalVariable->redirects[counting]->args[0][1] == '/'){
				char* tempNewPath = NULL;
				tempNewPath = getCurrentDir(tempNewPath);
				strcat(tempNewPath, (globalVariable->redirects[counting]->args[0])+1);
				printf("tempNewPath %s\n", tempNewPath);
				//printf("%s: command not found\n", globalVariable->new_args[0]);
				if(stat(tempNewPath, &buf) == -1) {
					foundPath = 0;
					printf("file is not here = %s\n", tempNewPath);
				} else {
					foundPath = 1;
					strcpy(executePath, tempNewPath);
				}
				if(foundPath == 1) {
					globalVariable->save_stdout = dup(1); // stdout = 1
					pid_t pid;
					pid = fork();
					usr_out = 1;
					if(pid == 0) {
						if(usr_in != 0) {
							dup2(usr_in, 0);
							close(usr_in);
						}
						if(usr_out != 1) {
							dup2(usr_out, 1);
							close(usr_out);
						}
						if(usr_err != 2) {
							dup2(usr_err, 2);
							close(usr_err);
						}
						//printf("%s: Excev path \n", executePath);
						// This is where I am executing new program
						if(execve(executePath, globalVariable->redirects[counting]->args , envp) == -1) {
							perror("error");
						}
						exit(0);
					} else if (pid < 0){
						perror("error");
						free(executePath);
						return -1;
					} else {
						globalVariable->now_pid = pid;
						dup2(globalVariable->save_stdout, 1);
						waitpid(pid,&status,0);
					}
					free(executePath);
					dup2(globalVariable->save_stdout, 1);
					return 0;
				}
				if(foundPath == 0) {
					printf("%s: command not found\n", globalVariable->redirects[counting]->args[0]);
					free(executePath);
					return 0;
				}
				free(tempNewPath);
			} else {
				char* tempNewPath = NULL;
				tempNewPath = getCurrentDir(tempNewPath);
				strcat(tempNewPath, "/");
				strcat(tempNewPath, (globalVariable->redirects[counting]->args[0])+1);
				printf("tempNewPath %s\n", tempNewPath);
				//printf("%s: command not found\n", globalVariable->new_args[0]);
				if(stat(tempNewPath, &buf) == -1) {
					foundPath = 0;
					printf("file is not here = %s\n", tempNewPath);
				} else {
					foundPath = 1;
					strcpy(executePath, tempNewPath);
				}
				if(foundPath == 1) {
					globalVariable->save_stdout = dup(1); // stdout = 1
					pid_t pid;
					pid = fork();
					usr_out = 1;
					if(pid == 0) {
						if(usr_in != 0) {
							dup2(usr_in, 0);
							close(usr_in);
						}
						if(usr_out != 1) {
							dup2(usr_out, 1);
							close(usr_out);
						}
						if(usr_err != 2) {
							dup2(usr_err, 2);
							close(usr_err);
						}
						//printf("%s: Excev path \n", executePath);
						// This is where I am executing new program
						if(execve(executePath, globalVariable->redirects[counting]->args , envp) == -1) {
							perror("error");
						}
						exit(0);
					} else if (pid < 0){
						perror("error");
						free(executePath);
						return -1;
					} else {
						globalVariable->now_pid = pid;
						dup2(globalVariable->save_stdout, 1);
						waitpid(pid,&status,0);
					}
					if(foundPath == 0) {
						printf("%s: command not found\n", globalVariable->redirects[counting]->args[0]);
					}
					free(executePath);
					dup2(globalVariable->save_stdout, 1);
					return 0;
				}
			}
		}
	}

	return 0;
}

char* getCurrentDir(char* save) {
	save = calloc(CALLOC_SIZE,sizeof(char));
	if(save == NULL){
		perror("error: Cannot Calloc\n");
	}
	getcwd(save, CALLOC_SIZE);
	return save;
}

// All signal handlers can be found here

void handle_ctrl_c(int signo) {
	if(signo == SIGINT) {
		printf("^C");
		fflush(stdout);
	}
	return;
}
void handle_easy(int signo) {
	fprintf(stdout, "Well that was easy\n");
	fflush(stdout);
}
void handle_child_death(int signo) {
	struct sigaction act;
	//act.sa_sigaction = malloc(sizeof(act.sa_sigaction));
	//memset(&act, '\0', sizeof(act));
	if(sigaction(SIGTERM, &act, NULL) < 0) {
		perror("error: sigaction ");
	}
	if(signo == SIGCHLD) {
		//printf("Child with the PID %i has died. It spent miliseconds utilizing the CPU\n", getpid());
	}
	fflush(stdout);
}
void do_nothing_sig(int signo) {
	fflush(stdout);
	return;
}
void print_alarm(int signo) {
	if(signo == SIGALRM) {
		printf("Your %i second timer has finished", globalVariable2->timer);
	}
	fflush(stdout);
}
