#include "sfish.h"
#include "debug.h"

/*
 * As in previous hws the main function must be in its own file!
 */

int main(int argc, char const *argv[], char* envp[]) {
    /* DO NOT MODIFY THIS. If you do you will get a ZERO. */
    rl_catch_signals = 0;
    /* This is disable readline's default signal handlers, since you are going to install your own.*/

    signal(SIGINT, handle_ctrl_c);
    signal(SIGCHLD, handle_child_death);
    signal(SIGUSR2, handle_easy);
    signal(SIGTSTP, do_nothing_sig);
    signal(SIGALRM, print_alarm);
    signal(SIGSEGV, do_nothing_sig);

    GlobalVariable *globalVariable = malloc(sizeof(GlobalVariable));
    globalVariable->timer = 0;

    globalVariable->path_broken = parseToArray(getenv("PATH"), ":");
    //info("I am here%i\n", 5);
    globalVariable->updateDir = 0; // used as a tracker to knwo when to change the directory
    globalVariable->save_stdin = dup(0);
    globalVariable->save_stdout = dup(1);
    globalVariable->save_stderr = dup(2);
    globalVariable->last_dir_visited = calloc(CALLOC_SIZE,sizeof(char));
    getcwd(globalVariable->last_dir_visited, CALLOC_SIZE);
    //char **homeDir = parseDirectories(getenv("HOME"));
    char *cmd;
    //info("I am here%i\n", 5);
    Redirect **redirect;
    globalVariable->current_directory = calloc(CALLOC_SIZE,sizeof(char)); // CALLOC_SIZE = 1024 This directory is what I am getting later
    getcwd(globalVariable->current_directory, CALLOC_SIZE); // getting the current directory
    globalVariable->current_directory = globalVariable->current_directory;
    //info("I am here%i\n", 5);
    char *shellName = calloc(CALLOC_SIZE,sizeof(char)); // Current shell name format <netid> : <pwd> $
    strcpy(shellName, "mankhan : ");
    strcat(shellName, globalVariable->current_directory);
    strcat(shellName, " $ ");

    while((cmd = readline(shellName)) != NULL) {
        if (strcmp(cmd, "exit")==0)
            break;
        //char * cmd2 = calloc(CALLOC_SIZE, sizeof(char));
        //strcpy(cmd2, cmd);
        //char** arguments = getParsedArray(cmd);
        //globalVariable->new_args = arguments;
        redirect = getAdvancedArray(cmd);
        globalVariable->redirects = redirect;
        int meaw = 0;
        while(redirect[meaw] != NULL) {
            meaw++;
        }
        globalVariable->redirect_count = meaw;

        distributeArgument(globalVariable, envp);

        dup2(globalVariable->save_stdin, 0);
        dup2(globalVariable->save_stdout, 1);
        dup2(globalVariable->save_stderr, 2);

        if(globalVariable->updateDir == 1) {
            free(globalVariable->current_directory);
            free(shellName);
            globalVariable->current_directory = calloc(CALLOC_SIZE,sizeof(char)); // CALLOC_SIZE = 1024 This directory is what I am getting later
            getcwd(globalVariable->current_directory, CALLOC_SIZE); // getting the current directory
            shellName = calloc(CALLOC_SIZE,sizeof(char)); // Current shell name format <netid> : <pwd> $
            strcpy(shellName, "mankhan : ");
            strcat(shellName, globalVariable->current_directory);
            strcat(shellName, " $ ");
        }

        meaw = 0;
        while(redirect[meaw] != NULL) {
            int meaw2 = 0;
            while(redirect[meaw]->args[meaw2]!= NULL) {
                free(redirect[meaw]->args[meaw2]);
                meaw2++;
            }
            free(redirect[meaw]->input);
            free(redirect[meaw]->output);
            free(redirect[meaw]);
            meaw++;
        }
        fflush(stdout);

        //free(arguments);
        /*char* thing = test[0];
        int i = 0;
        while(thing != NULL) {
            info("test[%d] = %s\n", i, thing);
            i++;
            thing = test[i];
        }*/
        //printf("%s\n",cmd);
        /* All your debug print statements should use the macros found in debug.h */
        /* Use the `make debug` target in the makefile to run with these enabled. */
        //info("Length of command entered: %ld\n", strlen(cmd));
        /* You WILL lose points if your shell prints out garbage values. */
    }

    /* Don't forget to free allocated memory, and close file descriptors. */
    free(redirect);
    free(cmd);
    free(globalVariable->current_directory);
    free(globalVariable->last_dir_visited);
    free(globalVariable->path_broken);
    free(globalVariable->home_directory_broken);
    free(globalVariable);
    free(shellName);
    //exit(0);
    return EXIT_SUCCESS;
}
